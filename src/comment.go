package main

import (
	"github.com/jinzhu/gorm"
)

// Comment represents a single comment
type Comment struct {
	gorm.Model
	Author   string    `json:"author" gorm:"not null"`
	Body     string    `json:"body" gorm:"not null"`
	Path     string    `json:"path" gorm:"not null"`
	Depth    int       `json:"depth"`
	Children []Comment `gorm:"foreignkey:ParentID"`
	ParentID uint      `json:"parentId"`
}
