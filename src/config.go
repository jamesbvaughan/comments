package main

type config struct {
	BaseURL      string
	DatabaseFile string
}

func newConfig() (conf *config) {
	conf = &config{
		BaseURL:      getEnv("BASE_URL", "http://localhost:8001"),
		DatabaseFile: getEnv("DATABASE_FILENAME", "comments.db"),
	}
	return
}
