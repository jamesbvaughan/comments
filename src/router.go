package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

type router struct {
	*mux.Router
	BaseURL string
}

func (rtr router) getCommentsHandler(w http.ResponseWriter, r *http.Request) {
	var comments []Comment

	db.Where("path = ?", r.FormValue("path")).Find(&comments)

	body, err := json.Marshal(comments)
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func (rtr router) postCommentHandler(w http.ResponseWriter, r *http.Request) {
	comment := &Comment{
		Author: strings.TrimSpace(r.FormValue("author")),
		Body:   strings.TrimSpace(r.FormValue("body")),
		Path:   strings.TrimSpace(r.FormValue("path")),
	}

	parentID := r.FormValue("parentID")
	if parentID != "" {
		var parent *Comment
		db.First(&parent, parentID)
		db.Model(&parent).Association("Children").Append(comment)
	} else {
		db.Create(comment)
	}

	body, err := json.Marshal(comment)
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func (rtr router) setHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", rtr.BaseURL)
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s\n", r.Method, r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func newRouter(baseURL string) (r *router) {
	r = &router{
		mux.NewRouter(),
		baseURL,
	}

	r.PathPrefix("/assets/").Handler(
		http.StripPrefix("/assets/",
			http.FileServer(http.Dir("assets"))),
	)
	r.HandleFunc("/comment/all", r.getCommentsHandler).Methods("GET")
	r.HandleFunc("/comment", r.postCommentHandler).Methods("POST")
	r.Use(r.setHeaders)
	r.Use(logger)
	return
}
