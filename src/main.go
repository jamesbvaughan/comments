package main

import (
	"log"
	"net/http"
)

var db *database

func seed() {
	parent := &Comment{
		Author: "bob",
		Body:   "nice",
		Path:   "/",
	}
	child := &Comment{
		Author: "jeff",
		Body:   "yeah",
		Path:   "/",
		Depth:  1,
	}
	third := &Comment{
		Author: "tim",
		Body:   "wow",
		Path:   "/",
	}

	db.Create(parent)
	db.Create(third)

	db.Model(&parent).Association("Children").Append(child)
}

func main() {
	conf := newConfig()
	router := newRouter(conf.BaseURL)
	db = newDatabase(conf.DatabaseFile)
	defer db.Close()

	seed()

	log.Fatal(http.ListenAndServe(":8000", router))
}
