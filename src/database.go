package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type database struct {
	*gorm.DB
}

func newDatabase(databaseFile string) (db *database) {
	gormDB, err := gorm.Open("sqlite3", databaseFile)
	if err != nil {
		panic(err)
	}

	db = &database{gormDB}

	db.LogMode(true)

	db.DropTableIfExists(&Comment{})
	db.AutoMigrate(&Comment{})

	return
}
