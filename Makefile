BUILD_DIR=build
SRC_FILES=$(wildcard src/*)

OUTPUT_FILE=$(BUILD_DIR)/comments

$(OUTPUT_FILE): $(SRC_FILES)
	@mkdir -p $$(dirname $@)
	go build -o $@ $^

clean:
	rm -r $(BUILD_DIR)

.PHONY: clean
